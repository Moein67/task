<?php

namespace Tests\Feature;

use App\Models\Service;
use App\Models\Ticket;
use App\Models\User;
use App\Notifications\TicketStatusChanged;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;

class TicketTest extends TestCase
{
    use RefreshDatabase;


    public function test_admin_user_can_see_all_tickets()
    {
        // Create an admin user and authenticate
        $adminUser = User::factory()->create(['is_admin' => true]);
        $this->actingAs($adminUser);

        // Create some tickets
        $tickets = Ticket::factory()->count(5)->create();

        // Make a GET request to the index method
        $response = $this->getJson('/api/tickets');

        // Assert that the response has a successful status code
        $response->assertOk()
            ->assertJsonCount(5, 'data')
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id',
                        'user_id',
                        'service_id',
                        'status',
                        'user',
                        'service',
                    ],
                ],
            ]);

        // Assert that the response contains the ticket resources
        foreach ($tickets as $ticket) {
            $response->assertJsonFragment([
                'id' => $ticket->id,
            ]);
        }
    }

    public function test_non_admin_user_can_only_see_own_tickets()
    {
        // Create a non-admin user and authenticate
        $nonAdminUser = User::factory()->create(['is_admin' => false]);
        $this->actingAs($nonAdminUser);

        // Create some tickets for the non-admin user
        $ownTickets = Ticket::factory()->count(3)->create(['user_id' => $nonAdminUser->id]);

        // Create some tickets for other users
        $otherTickets = Ticket::factory()->count(2)->create();

        // Make a GET request to the index method
        $response = $this->getJson('/api/tickets');

        // Assert that the response has a successful status code
        $response->assertOk()
            ->assertJsonCount(3, 'data')
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id',
                        'user_id',
                        'service_id',
                        'status',
                        'user',
                        'service',
                    ],
                ],
            ]);

        // Assert that the response contains only the ticket resources for the non-admin user
        foreach ($ownTickets as $ticket) {
            $response->assertJsonFragment([
                'id' => $ticket->id,
            ]);
        }
    }



    public function test_user_can_get_a_ticket()
    {
        // Create a user and authenticate
        $user = User::factory()->create();
        $this->actingAs($user);

        // Create a ticket
        $ticket = Ticket::factory()->create();

        // Make a GET request to the show method
        $response = $this->get('/api/tickets/' . $ticket->id);

        // Assert that the response has a successful status code
        $response->assertOk()
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'user_id',
                    'service_id',
                    'status',
                    'user',
                    'service',
                ]
            ]);

        // Assert that the response contains the ticket resource
        $response->assertJson([
            'data' => [
                'id' => $ticket->id,
            ]
        ]);

    }

    public function test_admin_user_can_change_ticket_status_to_seen()
    {
        Notification::fake();
        
        // Create an admin user and authenticate
        $adminUser = User::factory()->create(['is_admin' => true]);
        $this->actingAs($adminUser);

        // Create a pending ticket
        $ticket = Ticket::factory()->create();

        // Make a GET request to the show method
        $response = $this->get('/api/tickets/' . $ticket->id);

        // Assert that the response has a successful status code
        $response->assertOk()
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'user_id',
                    'service_id',
                    'status',
                    'user',
                    'service',
                ]
            ]);

        // Assert that the ticket status is changed to "seen"
        $this->assertDatabaseHas('tickets', [
            'id'     => $ticket->id,
            'status' => 'seen',
        ]);

        Notification::assertSentTo($ticket->user, TicketStatusChanged::class);
    }

    public function test_user_can_create_ticket()
    {
        $service = Service::factory()->create();

        // Create a user and authenticate
        $user = User::factory()->create();
        $this->actingAs($user);

        // Make a POST request to the store method
        $response = $this->post('/api/tickets', [
            'service_id' => $service->id,
        ]);

        // Assert that the response has a successful status code
        $response->assertOk();

        // Assert that the ticket is created in the database
        $this->assertDatabaseHas('tickets', [
            'user_id' => $user->id,
            'service_id' => $service->id,
        ]);
    }

    public function test_admin_can_cancel_ticket()
    {
        Notification::fake();

        // Create a user and authenticate
        $user = User::factory()->create(['is_admin' => true]);
        $this->actingAs($user);

        // Create a ticket
        $ticket = Ticket::factory()->create();

        //first show ticket
        $response = $this->get('/api/tickets/' . $ticket->id );


        // Make a POST request to the cancel method
        $response = $this->post('/api/tickets/' . $ticket->id . '/cancel');

        // Assert that the response has a successful status code
        $response->assertOk();

        // Assert that the ticket status is changed to CancelState
        $this->assertDatabaseHas('tickets', [
            'id'     => $ticket->id,
            'status' => Ticket::STATUS_CANCEL,
        ]);

        Notification::assertSentTo($ticket->user, TicketStatusChanged::class);
    }

    public function test_admin_can_complete_ticket()
    {
        Notification::fake();

        // Create a user and authenticate
        $user = User::factory()->create(['is_admin' => true]);
        $this->actingAs($user);

        // Create a ticket
        $ticket = Ticket::factory()->create();

        //first show ticket
        $response = $this->get('/api/tickets/' . $ticket->id );


        // Make a POST request to the complete method
        $response = $this->post('/api/tickets/' . $ticket->id . '/complete');

        // Assert that the response has a successful status code
        $response->assertOk();

        // Assert that the ticket status is changed to CancelState
        $this->assertDatabaseHas('tickets', [
            'id'     => $ticket->id,
            'status' => Ticket::STATUS_COMPLETED,
        ]);

        Notification::assertSentTo($ticket->user, TicketStatusChanged::class);
    }
}


