<?php

namespace Tests\Feature;

use App\Models\User;
use App\Notifications\VerificationCode;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;

class AuthTest extends TestCase
{
    use RefreshDatabase;

    public function test_register_sends_verification_code()
    {
        Notification::fake();

        $userData = [
            'email' => 'john@example.com',
        ];

        $response = $this->postJson('/api/register', $userData);

        $response->assertOk()
            ->assertJson(['message' => 'Verify code sent']);

        $user = User::where('email', $userData['email'])->first();
        $this->assertNotNull($user);

        Notification::assertSentTo($user, VerificationCode::class);
    }

    public function test_verify_with_valid_credentials()
    {
        $user = User::factory()->create();
        $verifyCode = $user->generateVerifyCode();

        $response = $this->postJson('/api/verify', [
            'email' => $user->email,
            'code' => $verifyCode,
        ]);

        $response->assertOk()
            ->assertJsonStructure([
                'authorization' => [
                    'token',
                    'type',
                ],
            ]);

        $user->refresh();
        $this->assertTrue(boolval($user->is_verified));
    }

    public function test_verify_with_invalid_credentials()
    {
        $user = User::factory()->create();
        $user->generateVerifyCode();

        $response = $this->postJson('/api/verify', [
            'email' => $user->email,
            'code' => '1234',
        ]);

        $response->assertStatus(401)
            ->assertJson(['message' => 'Invalid credentials']);

        $user->refresh();
        $this->assertFalse(boolval($user->is_verified));
    }
}
