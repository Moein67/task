<?php

namespace App\Models;

use App\Models\States\State;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'service_id',
        'status',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
 
    public function service()
    {
        return $this->belongsTo(Service::class);
    }

    const STATUS_PENDING   = 'pending';
    const STATUS_SEEN      = 'seen';
    const STATUS_CANCEL    = 'cancel';
    const STATUS_COMPLETED = 'completed';
    const STATUS_CLOSE     = 'closed';


    public function setStatus(State $state)
    {
        $this->status = $state->getName();
        $this->save();
    }

    public function changeStatusTo(State $state)
    {
        $state->apply($this);
    }
 
}
