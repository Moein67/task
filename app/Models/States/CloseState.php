<?php

namespace App\Models\States;

use App\Models\Ticket;
use App\Notifications\TicketStatusChanged;

class CloseState implements State
{
    public function getName()
    {
        return Ticket::STATUS_CLOSE;
    }

    public function apply(Ticket $ticket)
    {
        if ($this->isTransitionAllowed($ticket->status)) {
            $ticket->setStatus($this);

        } else {
            throw new \Exception("Transition to '{$this->getName()}' is not allowed from '{$ticket->status}'.");
        }
    }

    public function isTransitionAllowed(string $currentStatus): bool
    {
        return in_array($currentStatus, [
            Ticket::STATUS_COMPLETED,
            Ticket::STATUS_CANCEL,
        ]);
    }
}