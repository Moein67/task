<?php

namespace App\Models\States;

use App\Models\Ticket;
use App\Notifications\TicketStatusChanged;

class CompleteState implements State
{
    public function getName()
    {
        return Ticket::STATUS_COMPLETED;
    }

    public function apply(Ticket $ticket)
    {
        if ($this->isTransitionAllowed($ticket->status)) {
            $ticket->setStatus($this);

            //Send notification to user about the status change
            $ticket->user->notify(new TicketStatusChanged($ticket));
        } else {
            throw new \Exception("Transition to '{$this->getName()}' is not allowed from '{$ticket->status}'.");
        }
    }

    public function isTransitionAllowed(string $currentStatus): bool
    {
        return in_array($currentStatus, [
            Ticket::STATUS_SEEN,
        ]);
    }
}