<?php

namespace App\Models\States;

use App\Models\Ticket;

interface State
{
    public function getName();
    public function apply(Ticket $ticket);
    public function isTransitionAllowed(string $status): bool;
}
