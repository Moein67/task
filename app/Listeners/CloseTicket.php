<?php

namespace App\Listeners;

use App\Events\TicketCompleted;
use App\Models\States\CloseState;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CloseTicket implements ShouldQueue
{
    use InteractsWithQueue;
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(TicketCompleted $event)
    {
        $event->ticket->changeStatusTo(new CloseState());
    }
}
