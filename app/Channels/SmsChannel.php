<?php
 
namespace App\Channels;

use App\Interfaces\SMSInterface;
use Illuminate\Notifications\Notification;
 
class SmsChannel
{
    protected $smsSender;

    public function __construct(SMSInterface $smsSender)
    {
        $this->smsSender = $smsSender;
    }
    
    /**
     * Send the given notification.
     */
    public function send(object $notifiable, Notification $notification): void
    {
        $message = $notification->toSms($notifiable);
 
        // Send notification to the $notifiable instance...
        if ($notifiable->mobile) {
            $this->smsSender->send($notifiable->mobile, $message);
        }
    }
}