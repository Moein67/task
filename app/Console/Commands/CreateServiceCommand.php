<?php

namespace App\Console\Commands;

use App\Models\Service;
use Illuminate\Console\Command;

class CreateServiceCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'service:create {--count=1 : The number of services to create}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new service model with a sample name and description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $count = (int) $this->option('count');

        Service::factory()->count($count)->create();

        $this->info("Services created: $count");
    }
}
