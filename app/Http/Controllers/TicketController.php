<?php

namespace App\Http\Controllers;

use App\Events\TicketCompleted;
use App\Http\Controllers\Controller;
use App\Http\Requests\TicketRequest;
use App\Http\Resources\TicketResource;
use App\Models\States\CancelState;
use App\Models\States\CompleteState;
use App\Models\States\SeenState;
use App\Models\Ticket;

class TicketController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin')->only(['cancel','complete']);
    }

    public function index()
    {
        $tickets = Ticket::with('user','service')
        ->where(function ($q){
            if (!request()->user()->isAdmin()) {
                $q->where('user_id',auth()->id());
            }
        })->get();

        return TicketResource::collection($tickets);
    }


    public function show(Ticket $ticket)
    {
        if (request()->user()->isAdmin()) {
            $ticket->changeStatusTo(new SeenState());
        }

        return  new TicketResource($ticket);
    }


    public function store(TicketRequest $request)
    {
        Ticket::create([
            'user_id'    => auth()->id(),
            'service_id' => $request->input('service_id'),
        ]);
        
        return response()->json(['message' => __('ticket.create')]);
    }


    public function cancel(Ticket $ticket)
    {
        $ticket->changeStatusTo(new CancelState());

        return response()->json(['message' => __('ticket.cancel')]);
    }


    public function complete(Ticket $ticket)
    {
        $ticket->changeStatusTo(new CompleteState());

        event(new TicketCompleted($ticket));

        return response()->json(['message' => __('ticket.complete')]);
    }

}
