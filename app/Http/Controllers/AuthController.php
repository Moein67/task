<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Requests\Auth\VerifyRequest;
use App\Notifications\VerificationCode;
use App\Services\UserService;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function __construct(private UserService $userService)
    {
    }

    public function register(RegisterRequest $request)
    {
        $user = $this->userService->findOrNewUser($request);
        
        $verifyCode = $user->generateVerifyCode();

        $user->notify(new VerificationCode($verifyCode));

        return response()->json(['message' => 'Verify code sent']);
    }

    public function verify(VerifyRequest $request)
    {
        $user = $this->userService->findUser($request);
        if ($user->verifyCode->code == $request->input('code')) {
            
            $user->is_verified = true;
            $user->save();

            return response()->json([
                'authorization' => [
                    'token' => $user->createToken('ApiToken')->plainTextToken,
                    'type'  => 'bearer',
                ],
            ]);
        }

        return response()->json(['message' => 'Invalid credentials'], 401);
    }
}
