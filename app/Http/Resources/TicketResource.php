<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class TicketResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id'         => $this->id,
            'user_id'    => $this->user_id,
            'service_id' => $this->service_id,
            'status'     => $this->status,
            'user'       => new UserResource($this->user),
            'service'    => new ServiceResource($this->service),
            
        ];
    }
}
