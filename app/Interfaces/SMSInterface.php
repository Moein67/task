<?php

namespace App\Interfaces;

interface SMSInterface
{
    public function send(string $phoneNumber, string $message);
}