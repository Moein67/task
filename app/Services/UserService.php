<?php

namespace App\Services;

use App\Models\User;

class UserService
{
    public function findOrNewUser($data)
    {
        $user = $this->findUser($data);

        if (!$user) {
            $user = User::create([
                'mobile' => $data['mobile'],
                'email'  => $data['email'],
            ]);
        }
        return $user;
    }

    public function findUser($data)
    {
        $user = User::with('verifyCode')
            ->when($data['mobile'], function ($query) use ($data) {
                $query->where('mobile', $data['mobile']);
            })
            ->when($data['email'], function ($query) use ($data) {
                $query->where('email', $data['email']);
            })
            ->first();

        return $user;
    }
}
